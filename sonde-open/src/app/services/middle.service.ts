import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

interface MiddleModel {
  data: string;
  date: string;
}


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'https://localhost:443' }),
  withCredentials: true
};

@Injectable({
  providedIn: 'root'
})
export class MiddleService {

  constructor(private http: HttpClient) { }

/**
 * getTemperature
 * Recupère tous les données en base de données
 */
getTemperature(): Observable<Array<MiddleModel>> {
  return  this.http.get<Array<MiddleModel>>('http://127.0.0.1:443/Temperature').pipe(
    tap(_ => console.log('fetched temperature')));
}
/**
 * getPresure
 * Recupère tous les données en base de données
 */
getPresure(): Observable<Array<MiddleModel>> {
  return  this.http.get<Array<MiddleModel>>('http://127.0.0.1:443/presure').pipe(
    tap(_ => console.log('fetched presure')));
}
/**
 * getQuality
 * Recupère tous les données en base de données
 */
getQuality(): Observable<Array<MiddleModel>> {
  return  this.http.get<Array<MiddleModel>>('http://127.0.0.1:443/quality').pipe(
    tap(_ => console.log('fetched quality')));
}
/**
 * getGps
 * Recupère tous les données en base de données
 */
getGps(): Observable<Array<MiddleModel>> {
  return  this.http.get<Array<MiddleModel>>('http://127.0.0.1:443/gps').pipe(
    tap(_ => console.log('fetched gps')));
}
}
