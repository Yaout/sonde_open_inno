import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgChartjsModule } from 'ng-chartjs';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FrontInfoComponent } from './components/front-info/front-info.component';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TemperatureComponentComponent } from './components/temperature-component/temperature-component.component';
import { PresureComponentComponent } from './components/presure-component/presure-component.component';
import { QualityComponentComponent } from './components/quality-component/quality-component.component';
import { GpsDataComponentComponent } from './components/gps-data-component/gps-data-component.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';


@NgModule({
  declarations: [
    AppComponent,
    FrontInfoComponent,
    TemperatureComponentComponent,
    PresureComponentComponent,
    QualityComponentComponent,
    GpsDataComponentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgChartjsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
