import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PresureComponentComponent } from './presure-component.component';

describe('PresureComponentComponent', () => {
  let component: PresureComponentComponent;
  let fixture: ComponentFixture<PresureComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresureComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PresureComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
