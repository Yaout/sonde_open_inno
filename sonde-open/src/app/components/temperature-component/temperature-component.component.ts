import { Component, OnInit } from '@angular/core';
import { MiddleService } from 'src/app/services/middle.service';
import { FunctionUtilsService } from 'src/app/utils/functions/function-utils.service';

@Component({
  selector: 'app-temperature-component',
  templateUrl: './temperature-component.component.html',
  styleUrls: ['./temperature-component.component.scss'],
})
export class TemperatureComponentComponent implements OnInit {
  dataSource = [];

  // configuration of the graph
  lineChartData: Chart.ChartDataSets[] = [
    {
      label: 'Temperature',
      fill: false,
      lineTension: 0.1,
      backgroundColor: 'rgba(75,192,192,0.4)',
      borderColor: 'rgba(75,192,192,1)',
      borderCapStyle: 'butt',
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: 'rgba(75,192,192,1)',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: 'rgba(75,192,192,1)',
      pointHoverBorderColor: 'rgba(220,220,220,1)',
      pointHoverBorderWidth: 2,
      pointRadius: 10,
      pointHitRadius: 100,
      data: [],
    },
  ];
  lineChartLabels: Array<any> = [];
  lineChartOptions: any = {
    responsive: true,
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: '°C'
          },
          ticks: {
            beginAtZero: true,
          },
        },
      ], xAxes: [{
        scaleLabel: {
          display: true,
          labelString: 'Date'
        },
      }]
    },
    legend: { position: 'right' },
  };
  lineChartLegend = true;
  lineChartType = 'line';
  inlinePlugin: any;
  textPlugin: any;

  constructor(private middle: MiddleService, private utils: FunctionUtilsService) {}

  ngOnInit(): void {
    // Add data to the graph
    this.pushOne();
    
    // reload every 30 sec the graph to gather data
    setInterval(() => {
      this.pushOne();
      this.inlinePlugin = this.utils.reload(this.textPlugin);
    }, 30000);

    this.inlinePlugin = this.utils.reload(this.textPlugin);
  }

  // Add data to the graph
  public pushOne() {
    // Call the middle service to gather data from API
    this.middle.getTemperature().subscribe((data) => {
      console.log(data);
      this.dataSource = data;
      data.forEach((element) => {
        this.lineChartLabels = [];
        this.lineChartData.forEach((x, i) => {
          const num = parseInt(element.data);
          let data1: number[] = x.data as number[];
          data1.splice(0, data1.length);
        });
      });

      data.forEach((element) => {
        this.lineChartLabels.push(element.date);
        this.lineChartData.forEach((x, i) => {
          const num = parseInt(element.data);
          let data1: number[] = x.data as number[];
          console.log(num);
          // put data to data1 array in order to displays them
          data1.push(num);
        });
      });
      console.log(this.lineChartLabels);
    });
  }

  download() {
    // download the data from graph with a click on a button in the DOM
    this.utils.downloadCsvFile(this.dataSource, 'temperature-data');
  }
}
