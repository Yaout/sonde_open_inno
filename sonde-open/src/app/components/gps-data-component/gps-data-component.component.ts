import { Component, OnInit } from '@angular/core';
import { MiddleService } from 'src/app/services/middle.service';
import { FunctionUtilsService } from 'src/app/utils/functions/function-utils.service';

export interface PeriodicElement {
  timestamp: string;
  latitude: string;
  longitude: string;
  altitude: string;
  satellite: string;
  num: string;
}

@Component({
  selector: 'app-gps-data-component',
  templateUrl: './gps-data-component.component.html',
  styleUrls: ['./gps-data-component.component.scss'],
})
export class GpsDataComponentComponent implements OnInit {
  // ELEMENT_DATA: PeriodicElement[] = [];

  constructor(
    private middle: MiddleService,
    private utils: FunctionUtilsService
  ) {}

  displayedColumns: string[] = [
    'date',
    'latitude',
    'longitude',
    'altitude',
    'satellite',
    'num',
  ];
  // dataSource = this.ELEMENT_DATA;
  dataSource = [];

  ngOnInit(): void {
    this.pushOne();
    setInterval(() => {
      this.pushOne();
    }, 30000);
  }

  public pushOne() {
    this.middle.getGps().subscribe((data) => {
      console.log(data);
      this.dataSource = data;
    });
  }

  download() {
    this.utils.downloadCsvFile(this.dataSource, 'gps-data', [
      'date',
      'latitude',
      'longitude',
      'altitude',
      'satellite',
      'num',
    ]);
  }
}
