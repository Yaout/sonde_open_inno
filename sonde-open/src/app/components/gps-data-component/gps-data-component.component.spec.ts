import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GpsDataComponentComponent } from './gps-data-component.component';

describe('GpsDataComponentComponent', () => {
  let component: GpsDataComponentComponent;
  let fixture: ComponentFixture<GpsDataComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GpsDataComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GpsDataComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
