import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrontInfoComponent } from './front-info.component';

describe('FrontInfoComponent', () => {
  let component: FrontInfoComponent;
  let fixture: ComponentFixture<FrontInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrontInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrontInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
