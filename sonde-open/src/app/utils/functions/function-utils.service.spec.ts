import { TestBed } from '@angular/core/testing';

import { FunctionUtilsService } from './function-utils.service';

describe('FunctionUtilsService', () => {
  let service: FunctionUtilsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FunctionUtilsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
