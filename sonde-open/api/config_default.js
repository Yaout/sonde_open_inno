//copy this file to config.js and change user and password fields
var config = {
production: {
    database: {
        host: 'jenkins_db_1',
        port: '3306',
        user: 'bddadmin',
        password: 'password',
        db:   'REABDD'
    },
    server: {
        host:   '127.0.0.1',
        port:   '443'
    },
    keys:{
        RHKey: "thisistherhkey",
    }
}
};
module.exports = config;
