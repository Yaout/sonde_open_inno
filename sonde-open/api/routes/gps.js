module.exports = function (app) {
    var mysql = require('../services/db').pool;
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];
    var datetime = require('node-datetime');
    

    //get all gps coordinates
    app.get('/gps', function (req, res) {
        mysql.query("select * from gps", function (err, rows) {
            res.json(rows);
            console.log(rows);
        })
    });


    //post one gps coordinate
    app.post('/gps', (req, res, err) => {

        var dt = datetime.create(Date.now(), 'YYYY-MM-DD hh:mm:ss');
        mysql.query("INSERT INTO gps (date, latitude, longitude, altitude, satellite, num) VALUES('" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "', '" + 
        req.body.latitude + "', '"+ req.body.longitude + "', '"+ req.body.altitude + "', '"+ req.body.satellite + "', '"+ req.body.num +"');",
            function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }
            });
    });

}