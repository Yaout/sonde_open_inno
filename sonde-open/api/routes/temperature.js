module.exports = function (app) {
    var mysql = require('../services/db').pool;
    var env = process.env.NODE_ENV || 'production';
    var config = require('../config')[env];
    var datetime = require('node-datetime');
    

    //get all temperature measures
    app.get('/temperature', function (req, res) {
        mysql.query("select * from temperature", function (err, rows) {
            res.json(rows);
            console.log(rows);
        })
    });

    //post one temperature measure
    app.post('/temperature/:data', (req, res, err) => {
        var dt = datetime.create(Date.now(), 'YYYY-MM-DD hh:mm:ss');
        mysql.query("INSERT INTO temperature (data, date) VALUES('" + req.params.data + "', '" + new Date().toISOString().slice(0, 19).replace('T', ' ') + "');",
            function (err, rows) {
                if (!err) {
                    res.json({ "code": 200 });
                } else {
                    res.json({ "code": 500, "status": err });
                    return;
                }
            });
    });

}