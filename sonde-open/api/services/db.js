

var env = process.env.NODE_ENV || 'production';
var config = require('../config')[env];
const mysql = require('mysql2');


//connection to database
var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: config.database.host,
    port: config.database.port,
    user: config.database.user,
    password: config.database.password,
    database: config.database.db,
    debug: false
});


//display log if connected to database
pool.query('SELECT 1 + 1 AS solution', (error, results, fields) => {
    if (error) throw error;
    console.log('The solution is: ', results[0].solution);
  });

module.exports.pool = pool
