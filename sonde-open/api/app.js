
var express = require('express');
var https = require('http');
var fs = require('fs');
var env = process.env.NODE_ENV || 'production';
var config = require('./config')[env];
console.log(config);
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser')
var express = require('express')
var cors = require('cors')

require('events').EventEmitter.defaultMaxListeners = 200;


var hostname = config.server.host;
var port = config.server.port;


var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
var whitelist = ['http://localhost:4200']
var corsOptions = {
  origin: function (origin, callback) {
    if (whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true)
    } else {
      callback(new Error('Not allowed by CORS ' + origin))
    }
  },
  credentials: true,
  exposedHeaders: ['Content-Length', 'X-Foo', 'X-Bar'],
  method:['GET', 'PUT', 'POST', 'DELETE']
}

app.use(cors(corsOptions))

//create api server
https.createServer(app).listen(port, function () {
  console.log("The serveur running on https://" + hostname + ":" + port + "\n");
});


require('./routes')(app);